<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_index extends CI_Controller {
	 
	function __construct() { 
		parent::__construct(); 
		$this->load->database();
		$this->load->model("Model_admin");
		$this->load->helper('url');
			$this->load->helper('string');
          $this->load->library('form_validation');
	
		$this->load->library('session');
		$admin=$this->session->userdata('status');  
		if($admin=='' || $admin!='admin')	 
		{	
			redirect('Login');
		}

	}

	public function logout() {
		$this->session->unset_userdata('nama');
		$this->session->unset_userdata('status');
		session_destroy();
		redirect('Login');
	}


	public function index()
	{
		$data['hasil'] = $this->Model_admin->tampil_username();
		$this->load->view('admin/v_nav',$data);
    $data['karyawan'] = $this->db->get('karyawan')->num_rows();
    $data['jenis_barang'] = $this->db->get('jenis_barang')->num_rows();
    $data['supplier_barang'] = $this->db->get('supplier_barang')->num_rows();
    $data['warna_barang'] = $this->db->get('warna_barang')->num_rows();
		$this->load->view('admin/v_index',$data);
	}
/*----------------------------------------------------------------------------------------------
												Karyawan
-----------------------------------------------------------------------------------------------*/
	public function karyawan()
	{
		$data['hasil'] = $this->model_admin->tampil_username();
  $this->load->view('admin/v_nav',$data);
		$data['hasil'] = $this->model_admin->getKaryawan('karyawan',$data);
	
		$this->load->view('admin/v_karyawan',$data);
	}
	public function add_karyawan()
	{  $data['hasil'] = $this->model_admin->tampil_username();
		$this->load->view('admin/v_nav',$data);
		$this->load->view('admin/v_add_karyawan',$data);
		
	}
	public function add_aksi_karyawan()
    {

        $username = $this->input->post('username',$data);
      	$password = $this->input->post('password',$data);
        $status = $this->input->post('status',$data);

 		if($username ==''  || $status ==''||$password==''){
       		echo "<script>alert('Data tidak boleh kosong');history.go(-1);</script>";

       }
       else{
       		$data = array(
        'username' => $username,
        'password' => md5($password),
        'status' => $status

          );
     $this->model_admin->input_data($data,'karyawan');
     redirect('admin/c_index/karyawan');
       }
 	}

 	public function hapus_karyawan($id){
        $where = array('id_karyawan' => $id);
        $this->model_admin->hapus_data($where,'karyawan');
        redirect('admin/c_index/karyawan');
    }


   	public function edit_karyawan($id)
	{
      $data['hasil'] = $this->model_admin->tampil_username();
		$this->load->view('admin/v_nav',$data);

		$where = array('id_karyawan' => $id);
	 	$data['username']=$this->model_admin->edit_data($where,'karyawan')->result();

		$this->load->view('admin/v_edit_karyawan',$data);
	}

    public function update_karyawan(){
		$id = $this->input->post('id_karyawan');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
       	if($username =='' || $password ==''){
       		echo "<script>alert('Data tidak boleh kosong');history.go(-1);</script>";
       	}
       	else{
       		$data = array(
        	
            'username' => $username,
            'password' =>md5($password)
          
  			);
        $where = array('id_karyawan' => $id);

                $this->model_admin->update_data($where,$data,'karyawan');
                redirect('admin/c_index/karyawan');
       	}

        
       }


/*----------------------------------------------------------------------------------------------
												Barang_mentah
-----------------------------------------------------------------------------------------------*/
	public function barang_mentah()
	{
		$this->load->model('Model_admin');
		$data['mentah'] = $this->Model_admin->getbarangmentah('barang_mentah');
		$this->load->view('admin/v_nav');
		$this->load->view('admin/v_barang_mentah');
	}
/*----------------------------------------------------------------------------------------------
												Jenis Barang
-----------------------------------------------------------------------------------------------*/
	public function jenis_barang()
	{
		$data['hasil'] = $this->model_admin->tampil_username();
		$this->load->view('admin/v_nav',$data);
		$data['hasil'] = $this->model_admin->getjenisbarang('jenis_barang',$data);
		$this->load->view('admin/v_jenis_barang',$data);
	}

	public function add_jenis_barang()
	{
		$data['hasil'] = $this->model_admin->tampil_username();
		$this->load->view('admin/v_nav',$data);
		$this->load->view('admin/v_add_jenis_barang',$data);
		
	}

	public function add_aksi_jenis_barang()
    {

        $id_jenis_barang = $this->input->post('id_jenis_barang');
        $deskripsi_barang = $this->input->post('deskripsi_barang');

	 $this->form_validation->set_error_delimiters('<div id="error">', '</div>');
	 $this->form_validation->set_rules('id_jenis_barang','Id Barang', 'required|trim|required|callback_valid_nameJenisbarang');
 		
    if($this->form_validation->run() == FALSE){
       $ref = $this->input->server('HTTP_REFERER', TRUE);

    	$data['hasil'] = $this->model_admin->tampil_username();
		$this->load->view('admin/v_nav',$data);
		$this->load->view('admin/v_add_jenis_barang',$data);
    	

    }else{


 
       		$data = array(
        'id_jenis_barang' => $id_jenis_barang,
		'deskripsi_barang' => $deskripsi_barang      
          );
    $query= $this->model_admin->input_data($data,'jenis_barang');

          if($query!== FALSE){

                 $this->session->set_flashdata("pesan", 
                 	"   <div class=\"alert alert-success alert-dismissable\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"
                                                        aria-hidden=\"true\">×
                                                </button>
                                                <strong>Nama Barang Berhasil Ditambah</strong>
                                              
                                            </div> <div id=\"card-alert\" class=\"card green fade in\"> 
                    ");

                 redirect('admin/c_index/jenis_barang');

            }else{
                $ref = $this->input->server('HTTP_REFERER', TRUE);
                  redirect('admin/c_index/add_jenis_barang');

            }
   
       }
 	}


function valid_nameJenisbarang($id_jenis_barang)
{
        if ($this->model_admin->valid_nameJenisbarang($id_jenis_barang) == TRUE)
        {
           $this->form_validation->set_message('valid_nameJenisbarang', "Nama jenis barang dengan nama <b> $id_jenis_barang  </b> sudah terdaftar");
           //  echo "<script>alert('Nama Client dengan Nama $nama_client sudah terdaftar');history.go(-1);</script>";
            return FALSE;
        }
        else
        {           
            return TRUE;
        }
}

  public function hapus_jenis_barang($id){
        $where = array('id_jenis_barang' => $id);
        $this->model_admin->hapus_data($where,'jenis_barang');
        redirect('admin/c_index/jenis_barang');
    }


    public function edit_jenis_barang($id)
  {
      $data['hasil'] = $this->model_admin->tampil_username();
    $this->load->view('admin/v_nav',$data);

    $where = array('id_jenis_barang' => $id);
    $data['jenis_barang']=$this->model_admin->edit_data($where,'jenis_barang')->result();

    $this->load->view('admin/v_edit_jenis_barang',$data);
  }

    public function update_jenisBarang(){
    $id = $this->input->post('id_jenis_barang');
        $deskripsi = $this->input->post('deskripsi_barang');
   

          $data = array(
          
            'id_jenis_barang' => $id,
            'deskripsi_barang' =>$deskripsi
          
        );
        $where = array('id_jenis_barang' => $id);

                $this->model_admin->update_data($where,$data,'jenis_barang');
                redirect('admin/c_index/jenis_barang');
        

        
       }



/*----------------------------------------------------------------------------------------------
												Supplier Barang
-----------------------------------------------------------------------------------------------*/
	public function supplier()
		{
			$data['hasil'] = $this->Model_admin->tampil_username();
			$this->load->view('admin/v_nav',$data);
			$data['hasil'] = $this->Model_admin->getsupplier();
			$this->load->view('admin/v_supplier',$data);
		}
	public function add_supplier()
		{
			$data['hasil'] = $this->Model_admin->tampil_username();
			$this->load->view('admin/v_nav',$data);
			$this->load->view('admin/v_add_supplier',$data);
			
		}
	
	public function add_aksi_supplier()
    	{
        	$nama_supplier = $this->input->post('nama_supplier');
			$this->form_validation->set_error_delimiters('<div id="error">', '</div>');
			$this->form_validation->set_rules('nama_supplier','Nama Supplier', 'required|trim|required|callback_valid_nameSupplier');
		 		
		    if($this->form_validation->run() == FALSE){
		       $ref = $this->input->server('HTTP_REFERER', TRUE);

	    	$data['hasil'] = $this->Model_admin->tampil_username();
			$this->load->view('admin/v_nav',$data);
			$this->load->view('admin/v_add_supplier',$data);
    	

    	}
    	else
    	{
       		$data = array('nama_supplier' => $nama_supplier);
    		$query= $this->Model_admin->input_data($data,'supplier_barang');

          	if($query!== FALSE){

            $this->session->set_flashdata("pesan", 
                 	"   <div class=\"alert alert-success alert-dismissable\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"
                                                        aria-hidden=\"true\">×
                                                </button>
                                                <strong>Nama Supplier Berhasil Ditambah</strong>
                                              
                                            </div> <div id=\"card-alert\" class=\"card green fade in\"> 
                    ");

            redirect('admin/c_index/supplier');

            }
            else
            {
                $ref = $this->input->server('HTTP_REFERER', TRUE);
                redirect('admin/c_index/add_supplier');
            }
   
       }
 	}

 	// cek apakah nama warna sudah ada
function valid_nameSupplier($nama_supplier)
{
        if ($this->Model_admin->valid_nameSupplier($nama_supplier) == TRUE)
        {
           $this->form_validation->set_message('valid_nameSupplier', "Nama Supplier dengan nama <b> $nama_supplier  </b> sudah terdaftar");
           
            return FALSE;
        }
        else
        {           
            return TRUE;
        }
}

	public function hapus_supplier($id)
 		{
        	$where = array('id_supplier' => $id);
        	$this->Model_admin->hapus_data($where,'supplier_barang');
        	redirect('admin/c_index/supplier');
    	}
	public function edit_supplier($id)
		{	$data['hasil'] = $this->Model_admin->tampil_username();
			$this->load->view('admin/v_nav',$data);
			$where = array('id_supplier' => $id);
		 	$data['nama_supplier']=$this->Model_admin->edit_data($where,'supplier_barang')->result();
			$this->load->view('admin/v_edit_supplier',$data);
		}
	public function update_supplier()
		{
			$id = $this->input->post('id_supplier');
        	$nama_supplier = $this->input->post('nama_supplier');
   
       		$data = array(
            'nama_supplier' => $nama_supplier
  			);
        	$where = array('id_supplier' => $id);

                $this->Model_admin->update_data($where,$data,'supplier_barang');
                redirect('admin/c_index/supplier');
       	// }

        
       }
/*----------------------------------------------------------------------------------------------
												Warna untuk Barang
-----------------------------------------------------------------------------------------------*/
	public function Warna()
	{
		$data['hasil'] = $this->Model_admin->tampil_username();
		$this->load->view('admin/v_nav',$data);
		$data['hasil'] = $this->Model_admin->getwarna();
		$this->load->view('admin/v_warna',$data);
	}
	public function add_warna()
	{
		$data['hasil'] = $this->Model_admin->tampil_username();
		$this->load->view('admin/v_nav',$data);
		$this->load->view('admin/v_add_warna',$data);
		
	}
	public function add_aksi_warna()
    {

     $warna = $this->input->post('warna');

	 $this->form_validation->set_error_delimiters('<div id="error">', '</div>');
	 $this->form_validation->set_rules('warna','Nama Warna', 'required|trim|required|callback_valid_nameWarna');
 		
    if($this->form_validation->run() == FALSE){
       $ref = $this->input->server('HTTP_REFERER', TRUE);

    	$data['hasil'] = $this->Model_admin->tampil_username();
		$this->load->view('admin/v_nav',$data);
		$this->load->view('admin/v_add_warna',$data);
    	

    }else{


 
       		$data = array(
        'warna' => $warna
          );
    $query= $this->Model_admin->input_data($data,'warna_barang');

          if($query!== FALSE){

                 $this->session->set_flashdata("pesan", 
                 	"   <div class=\"alert alert-success alert-dismissable\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"
                                                        aria-hidden=\"true\">×
                                                </button>
                                                <strong>Tambah Warna Berhasil</strong>
                                              
                                            </div> <div id=\"card-alert\" class=\"card green fade in\"> 
                    ");

                 redirect('admin/c_index/warna');

            }else{
                $ref = $this->input->server('HTTP_REFERER', TRUE);
                  redirect('admin/c_index/add_warna');

            }
   
       }
 	}

 	// cek apakah nama warna sudah ada
function valid_nameWarna($nama_warna)
{
        if ($this->Model_admin->valid_nameWarna($nama_warna) == TRUE)
        {
           $this->form_validation->set_message('valid_nameWarna', "Nama Warna dengan nama <b> $nama_warna  </b> sudah terdaftar");
           
            return FALSE;
        }
        else
        {           
            return TRUE;
        }
}

 	public function hapus_warna($id)
 	{
        $where = array('id_warna' => $id);
        $this->Model_admin->hapus_data($where,'warna_barang');
        redirect('admin/c_index/warna');
    }

    public function edit_warna($id)
	{	$data['hasil'] = $this->Model_admin->tampil_username();
		$this->load->view('admin/v_nav',$data);
		$where = array('id_warna' => $id);
	 	$data['warna']=$this->Model_admin->edit_data($where,'warna_barang')->result();
		$this->load->view('admin/v_edit_warna',$data);
	}	

	public function update_warna(){
		$id = $this->input->post('id_warna');
        $warna_barang = $this->input->post('warna_barang');
       	if($warna_barang ==''){
       		echo "<script>alert('Data tidak boleh kosong');history.go(-1);</script>";
       	}
       	else{
       		$data = array(
        	
            'warna' => $warna_barang
        
           
  			);
        $where = array('id_warna' => $id);

                $this->Model_admin->update_data($where,$data,'warna_barang');
                redirect('admin/c_index/warna');
       	}

        
       }


	public function login()
	{
		$this->load->view('login');
	}
}
