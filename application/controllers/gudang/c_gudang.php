	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_gudang extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model("M_gudang");
		$this->load->helper('url');
		$this->load->helper('string');
		$this->load->library('session');
		$this->load->helper	('date');
		$this->load->library('form_validation');
		$gudang=$this->session->userdata('status');  
		if($gudang=='' || $gudang!='gudang')	 
		{	
			redirect('Login');
		}

	}


	public function index()
	{
		$data['hasil'] = $this->M_gudang->tampil_username();
		$this->load->view('gudang/v_nav',$data);
    $data['barang_masuk'] = $this->db->get('barang_masuk')->num_rows();
    $data['barang_mentah'] = $this->db->get('barang_mentah')->num_rows();
    $data['permintaan_barang'] = $this->db->get('permintaan_barang')->num_rows();
    $data['barang_jadi'] = $this->db->get('barang_jadi')->num_rows();
		$this->load->view('gudang/v_index',$data);
	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('status');
		session_destroy();
		redirect('Login');
	}

/*----------------------------------------------------------------------------------------------
												Barang_mentah
-----------------------------------------------------------------------------------------------*/

	public function barang_mentah()
	{	
		$data['hasil'] = $this->M_gudang->tampil_username();
		$this->load->view('gudang/v_nav',$data);
		$data['hasil'] = $this->M_gudang->getbarangmentah();
		$this->load->view('gudang/v_barang_mentah',$data);
	}

	public function hapus_barang_mentah($id){
        $where = array('id_barang_mentah' => $id);
        $this->model_admin->hapus_data($where,'barang_mentah');
        redirect('gudang/c_gudang/barang_mentah');
    }

    public function edit_barang_mentah($id)
	{
		$data['hasil'] = $this->M_gudang->tampil_username();
		$this->load->view('gudang/v_nav',$data);
		$where = array('id_barang_mentah' => $id);
	 	$data['username']=$this->M_gudang->edit_data($where,'barang_mentah')->result();
		$this->load->view('gudang/v_edit_barang_mentah',$data);
	}

    public function update_karyawan(){
		$id = $this->input->post('id_karyawan');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
       	if($username =='' || $password ==''){
       		echo "<script>alert('Data tidak boleh kosong');history.go(-1);</script>";
       	}
       	else{
       		$data = array(
        	
            'username' => $username,
            'password' =>md5($password),
          
  			);
        $where = array('id_karyawan' => $id);

                $this->model_admin->update_data($where,$data,'karyawan');
                redirect('admin/c_index/karyawan');
       	}

        
       }
/*----------------------------------------------------------------------------------------------
												Barang_masuk
-----------------------------------------------------------------------------------------------*/
	public function barang_masuk()
	{
		$data['hasil'] = $this->M_gudang->tampil_username();
		$this->load->view('gudang/v_nav',$data);
		$data['hasil'] = $this->M_gudang->getbarangmasuk();
		$this->load->view('gudang/v_barang_masuk',$data);
	}

	public function add_barang_masuk()
	{
		$data['hasil'] = $this->M_gudang->tampil_username();
		$data['getnamabarang'] = $this->M_gudang->jenis_barang();
		$data['getsupplier'] = $this->m_gudang->getsupplier();
		$data['getwarna'] = $this->m_gudang->getwarna();
		$this->load->view('gudang/v_nav',$data);
		$this->load->view('gudang/v_add_barang_masuk',$data);
		
		
	}
	public function add_aksi_barang_masuk()
    {

        $deskripsi_barang = $this->input->post('deskripsi_barang');
        $nama_supplier = $this->input->post('nama_supplier');
        $jumlah = $this->input->post('jumlah');
        $warna = $this->input->post('warna');

        $cek = "SELECT * FROM barang_mentah";
        $query=  $this->db->query($cek)->result();  
        foreach ($query as $row ){
            $jenis_barang = $row->id_jenis_barang;
          $id_warna=$row->id_warna;
        }

    
        //bikin query filter di tbl_barang mentah berdasarkan jenis barang dan warna
        //jika jenis barang dan warna yg di input sama dengan yang ada di barang mentah maka update stok doang, kalo engga insert semua field
        if($deskripsi_barang == $jenis_barang && $warna==$id_warna){

        		$this->M_gudang->update_barang_mentah($jumlah,$deskripsi_barang,$warna);
        

                $data = array(
        'id_jenis_barang' => $deskripsi_barang,
        'id_supplier' => $nama_supplier,
        'jumlah' => $jumlah,
        'id_warna' => $warna,
          );

      $query= $this->m_gudang->input_data($data,'barang_masuk');
          redirect('gudang/c_gudang/barang_masuk');

       	}
       	else{


       	$data = array(
        'id_jenis_barang' => $deskripsi_barang,
        'id_supplier' => $nama_supplier,
        'jumlah' => $jumlah,
        'id_warna' => $warna,
          );

   	 	$query= $this->m_gudang->input_data($data,'barang_masuk');

        $data2 = array(
        'id_jenis_barang' => $deskripsi_barang,
        'stok' => $jumlah,
        'id_warna' => $warna
          );

        $barang_mentah= $this->m_gudang->input_data($data2,'barang_mentah');

          if($barang_mentah !== FALSE){

                 $this->session->set_flashdata("pesan", 
                 	"  <div class=\"alert alert-success alert-dismissable\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"
                                                        aria-hidden=\"true\">×
                                                </button>
                                                <strong>Barang Berhasil ditambah</strong>
                                              
                                            </div> <div id=\"card-alert\" class=\"card green fade in\"> 
                    ");

                 redirect('gudang/c_gudang/barang_masuk');


            }else{
                $ref = $this->input->server('HTTP_REFERER', TRUE);
                  redirect('gudang/c_gudang/add_barang_masuk');



            }

         }
   
       
 	}

/*----------------------------------------------------------------------------------------------
                        Permintaan_barang
-----------------------------------------------------------------------------------------------*/
  public function permintaan_barang()
  {
    $data['hasil'] = $this->M_gudang->tampil_username();
    $this->load->view('gudang/v_nav',$data);
    $data['hasil'] = $this->M_gudang->getpermintaanbarang()->result_array();
    $this->load->view('gudang/v_permintaan_barang',$data);
  }

  public function batal($id=0)
  {
    $status = 0;
    // $where = array('id_permintaan_barang' => $id);
    // $data = array('status' => $status);
               $this->M_gudang->permintaan_batal($id_permintaan_barang);
                redirect('gudang/c_gudang/permintaan_barang');
  }

  public function formkonfirm_permintaan($id)
  {
    $data['hasil'] = $this->M_gudang->tampil_username();
    $this->load->view('gudang/v_nav',$data);
    $where=array('id_permintaan_barang'=>$id);
    $data['batal'] = $this->M_gudang->permintaan_barang($where)->result();
    $this->load->view('gudang/v_konfirmasi_batal', $data);
  }
  public function aksi_formkonfirm_permintaan(){
    $id_permintaan_barang=$this->input->post('id_permintaan_barang');
    $keterangan_tolak = $this->input->post('keterangan_tolak');
    $status=0;

   $data = array('status' => $status, 'keterangan_tolak'=> $keterangan_tolak );
   $where  = array('id_permintaan_barang' => $id_permintaan_barang );
   $this->M_gudang->update_data($where,$data,'permintaan_barang');
   redirect('gudang/c_gudang/Permintaan_barang');

  }

  public function konfirm_barang()
  {
    $id_barang_mentah = $this->input->post('id_barang_mentah');
    $id_permintaan_barang = $this->input->post('id_permintaan_barang');
    $jlh=$this->input->post('jumlah');
    $stok = $this->input->post('stok');
   

    if ($this->input->post('terima')) 
    {
       $this->M_gudang->permintaan_oke($jlh,$id_barang_mentah,$id_permintaan_barang,$stok);
        $data2 = array(
        'id_permintaan_barang' => $id_permintaan_barang
    
                  );
      $query= $this->m_gudang->input_data($data2,'pengiriman_barang_mentah');
       redirect('gudang/c_gudang/permintaan_barang');

    }
    else
    {
       $this->M_gudang->permintaan_batal($id_permintaan_barang);
       redirect('gudang/c_gudang/permintaan_barang');

    } 
    
  }

/*----------------------------------------------------------------------------------------------
                        Konfirmasi Barang Jadi
-----------------------------------------------------------------------------------------------*/
  public function konfirmasi_barang_jadi()
  {
    $data['hasil'] = $this->M_gudang->tampil_username();
    $this->load->view('gudang/v_nav',$data);
    $data['hasil'] = $this->M_gudang->getbarangjadi()->result();
    $this->load->view('gudang/v_konfirmasi_barang_jadi',$data);
  }

  public function aksi_konfirm_barangjadi()
  {
    $id_barang_jadi = $this->input->post('id_barang_jadi');

    if ($this->input->post('terima')) 
    {
       $this->M_gudang->konfirmasi_oke($id_barang_jadi);
    
     
       redirect('gudang/c_gudang/konfirmasi_barang_jadi');

    }
    else
    {
       $this->input->post('tolak');
       $this->M_gudang->konfirmasi_batal($id_barang_jadi);
         
       redirect('gudang/c_gudang/konfirmasi_barang_jadi');

    } 
    
  }
/*----------------------------------------------------------------------------------------------
												Warna untuk Barang
-----------------------------------------------------------------------------------------------*/
	public function Warna()
	{
		$data['hasil'] = $this->M_gudang->tampil_username();
		$this->load->view('gudang/v_nav',$data);
		$data['hasil'] = $this->M_gudang->getwarna();
		$this->load->view('gudang/v_warna',$data);
	}
	public function add_warna()
	{
		$data['hasil'] = $this->M_gudang->tampil_username();
		$this->load->view('gudang/v_nav',$data);
		$this->load->view('gudang/v_add_warna',$data);
		
	}
	public function add_aksi_warna()
    {

        $warna = $this->input->post('warna');

	 $this->form_validation->set_error_delimiters('<div id="error">', '</div>');
	 $this->form_validation->set_rules('warna','Nama Warna', 'required|trim|required|callback_valid_nameWarna');
 		
    if($this->form_validation->run() == FALSE){
       $ref = $this->input->server('HTTP_REFERER', TRUE);

    	$data['hasil'] = $this->M_gudang->tampil_username();
		$this->load->view('gudang/v_nav',$data);
		$this->load->view('gudang/v_add_warna',$data);
    	

    }else{


 
       		$data = array(
        'warna' => $warna
          );
    $query= $this->m_gudang->input_data($data,'warna_barang');

          if($query!== FALSE){

                 $this->session->set_flashdata("pesan", 
                 	"   <div class=\"alert alert-success alert-dismissable\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"
                                                        aria-hidden=\"true\">×
                                                </button>
                                                <strong>Nama Supplier Berhasil</strong>
                                              
                                            </div> <div id=\"card-alert\" class=\"card green fade in\"> 
                    ");

                 redirect('gudang/c_gudang/warna');

            }else{
                $ref = $this->input->server('HTTP_REFERER', TRUE);
                  redirect('gudang/c_gudang/add_warna');

            }
   
       }
 	}

 	// cek apakah nama warna sudah ada
function valid_nameWarna($nama_warna)
{
        if ($this->m_gudang->valid_nameWarna($nama_warna) == TRUE)
        {
           $this->form_validation->set_message('valid_nameWarna', "Nama Warna dengan nama <b> $nama_warna  </b> sudah terdaftar");
           
            return FALSE;
        }
        else
        {           
            return TRUE;
        }
}

 	public function hapus_warna($id)
 	{
        $where = array('id_warna' => $id);
        $this->m_gudang->hapus_data($where,'warna_barang');
        redirect('gudang/c_gudang/warna');
    }

    public function edit_warna($id)
	{	$data['hasil'] = $this->M_gudang->tampil_username();
		$this->load->view('gudang/v_nav',$data);
		$where = array('id_warna' => $id);
	 	$data['warna']=$this->m_gudang->edit_data($where,'warna_barang')->result();
		$this->load->view('gudang/v_edit_warna',$data);
	}	

	 public function update_warna(){
		$id = $this->input->post('id_warna');
        $warna_barang = $this->input->post('warna_barang');
       	if($warna_barang ==''){
       		echo "<script>alert('Data tidak boleh kosong');history.go(-1);</script>";
       	}
       	else{
       		$data = array(
        	
            'warna' => $warna_barang
        
          
  			);
        $where = array('id_warna' => $id);

                $this->m_gudang->update_data($where,$data,'warna_barang');
                redirect('gudang/c_gudang/warna');
       	}

        
       }
/*----------------------------------------------------------------------------------------------
												Supplier Barang
-----------------------------------------------------------------------------------------------*/
	public function supplier()
		{
			$data['hasil'] = $this->M_gudang->tampil_username();
			$this->load->view('gudang/v_nav',$data);
			$data['hasil'] = $this->M_gudang->getsupplier();
			$this->load->view('gudang/v_supplier',$data);
		}
	public function add_supplier()
		{
			$data['hasil'] = $this->M_gudang->tampil_username();
			$this->load->view('gudang/v_nav',$data);
			$this->load->view('gudang/v_add_supplier',$data);
			
		}
		public function add_aksi_supplier()
    	{
        	$nama_supplier = $this->input->post('nama_supplier');
			$this->form_validation->set_error_delimiters('<div id="error">', '</div>');
			$this->form_validation->set_rules('nama_supplier','Nama Supplier', 'required|trim|required|callback_valid_nameSupplier');
		 		
		    if($this->form_validation->run() == FALSE){
		       $ref = $this->input->server('HTTP_REFERER', TRUE);

	    	$data['hasil'] = $this->M_gudang->tampil_username();
			$this->load->view('gudang/v_nav',$data);
			$this->load->view('gudang/v_add_supplier',$data);
    	

    	}
    	else
    	{
       		$data = array('nama_supplier' => $nama_supplier);
    		$query= $this->m_gudang->input_data($data,'supplier_barang');

          	if($query!== FALSE){

            $this->session->set_flashdata("pesan", 
                 	"   <div class=\"alert alert-success alert-dismissable\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"
                                                        aria-hidden=\"true\">×
                                                </button>
                                                <strong>Nama Supplier Berhasil Ditambah</strong>
                                              
                                            </div> <div id=\"card-alert\" class=\"card green fade in\"> 
                    ");

            redirect('gudang/c_gudang/supplier');

            }
            else
            {
                $ref = $this->input->server('HTTP_REFERER', TRUE);
                redirect('gudang/c_gudang/add_supplier');
            }
   
       }
 	}

 	// cek apakah nama warna sudah ada
function valid_nameSupplier($nama_supplier)
{
        if ($this->m_gudang->valid_nameSupplier($nama_supplier) == TRUE)
        {
           $this->form_validation->set_message('valid_nameSupplier', "Nama Supplier dengan nama <b> $nama_supplier  </b> sudah terdaftar");
           
            return FALSE;
        }
        else
        {           
            return TRUE;
        }
}

public function hapus_supplier($id)
 	{
        $where = array('id_supplier' => $id);
        $this->m_gudang->hapus_data($where,'supplier_barang');
        redirect('gudang/c_gudang/supplier');
    }
public function edit_supplier($id)
	{	$data['hasil'] = $this->M_gudang->tampil_username();
		$this->load->view('gudang/v_nav',$data);
		$where = array('id_supplier' => $id);
	 	$data['nama_supplier']=$this->m_gudang->edit_data($where,'supplier_barang')->result();
		$this->load->view('gudang/v_edit_supplier',$data);
	}
public function update_supplier(){
		$id = $this->input->post('id_supplier');
        $nama_supplier = $this->input->post('nama_supplier');
       	// if($nama_supplier ==''){
       	// 	echo "<script>alert('Data tidak boleh kosong');history.go(-1);</script>";
       	// }
       	// else{
       		$data = array(
        	
            'nama_supplier' => $nama_supplier
        
          
  			);
        $where = array('id_supplier' => $id);

                $this->m_gudang->update_data($where,$data,'supplier_barang');
                redirect('gudang/c_gudang/supplier');
       	// }

        
       }
}