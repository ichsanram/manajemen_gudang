<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Model_login'); 
    $this->load->helper("url");
    $this->load->library('session');
    

  }


  public function index() {
      if ($this->session->userdata('status')=='admin') {
        redirect('admin/c_index/');
      }
      elseif ($this->session->userdata('status')=='gudang') {
        redirect('gudang/c_gudang/');
      }
      elseif ($this->session->userdata('status')=='produksi') {
        redirect('produksi/c_produksi/');
      }   
    $this->load->view('view_login');
  }

  public function cek_login() {
    $data = array('username' => $this->input->post('user', TRUE),
            'password' => md5($this->input->post('pass', TRUE))
      );
    
    $hasil = $this->model_login->cek_user($data);
    
    if ($hasil->num_rows() == 1)
     {
      foreach ($hasil->result() as $sess) {
        $sess_data['logged_in'] = 'Sudah Loggin';
        $sess_data['id_karyawan'] = $sess->id_karyawan;
        $sess_data['status'] = $sess->status;
        $sess_data['username'] = $sess->username;
        $this->session->set_userdata($sess_data);
      }
        if ($this->session->userdata('status')=='admin') {
        redirect('admin/c_index/');
      }
      elseif ($this->session->userdata('status')=='gudang') {
        redirect('gudang/c_gudang/');
      }
      elseif ($this->session->userdata('status')=='produksi') {
        redirect('produksi/c_produksi/');
      }   
    }
    else {
      echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
    }
  }


        // public function index(){
        //     $this->load->view('view_login');
    
        // }
        // public function ceklogin(){
        //    if(isset($_POST['login'])){
        //        $user = $this->input->post('user',true);
        //        $pass = $this->input->post('pass',true);
        //        $cek  = $this->model_login->proseslogin($user,$pass);
        //        $hasil= count($cek);
        //       if($hasil  > 0 ){
        //           $datalogin = $this->db->get_where('karyawan',array('username'=> $user, 'password' => $pass))-> row();
        //           if($datalogin->status == 'admin'){
        //             redirect('admin/c_index');
        //           }
        //           else if($datalogin->status == 'produksi'){
        //             redirect('c_produksi');
        //           }
        //           else if($datalogin->status == 'gudang'){
        //               redirect('gudang/c_gudang');
        //           }
        //         }
                  
        //           }else{
        //                redirect('login');
        //           } 
              
        //    }

        //    public function beranda(){
        //        $this->load->view('admin/c_index');
        //    }

        //    public function logout(){
        //        $this->session->sess_destroy();
               
        //        redirect('login');
               
        //    }
  }


