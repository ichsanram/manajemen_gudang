<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_produksi extends CI_Controller {
	 
	function __construct() {
		parent::__construct();
		$this->load->database(); 
		$this->load->model("M_produksi");
		$this->load->helper('url');
			$this->load->helper('string');
	
		$this->load->library('session');
			$this->load->helper	('date');
			$this->load->library('form_validation');
		$produksi=$this->session->userdata('status');  
		if($produksi=='' || $produksi!='produksi')	 
		{	
			redirect('Login');
		}

	}

	public function logout() {
		$this->session->unset_userdata('nama');
		$this->session->unset_userdata('status');
		session_destroy();
		redirect('Login');
	}


	public function index()
	{
		$data['hasil'] = $this->M_produksi->tampil_username();
		$this->load->view('produksi/v_nav',$data);
		$data['barang_mentah'] = $this->db->get('barang_mentah')->num_rows();
    	$data['permintaan_barang'] = $this->db->get('permintaan_barang')->num_rows();
    	$data['pengiriman_barang_mentah'] = $this->db->get('pengiriman_barang_mentah')->num_rows();
    	$data['barang_jadi'] = $this->db->get('barang_jadi')->num_rows();
		$this->load->view('produksi/v_index',$data);
	}
/*----------------------------------------------------------------------------------------------
												Barang Mentah
-----------------------------------------------------------------------------------------------*/
	public function barang_mentah()
	{
	
		
		$data['hasil'] = $this->M_produksi->tampil_username();
		$this->load->view('produksi/v_nav',$data);
		$data['hasil'] = $this->M_produksi->getbarangmentah();
		$this->load->view('produksi/v_barang_mentah',$data);
	}
	public function add_barang_mentah($id)
	{
	
		
		$data['hasil'] = $this->M_produksi->tampil_username();
		$this->load->view('produksi/v_nav',$data);
		$where = array('id_barang_mentah' => $id);
		$data['barang_mentah']=$this->M_produksi->getbarang_mentah($where)->result_array();
		$this->load->view('produksi/v_add_permintaan',$data);
	}

 	public function add_aksi_barangmentah()
    {
    	$id_barang_mentah=$this->input->post('id_barang_mentah');	
    	$stok=$this->input->post("stok");
    	$id_karyawan=$_SESSION['id_karyawan'];
    	$today = date('Y-m-d');
      	$jumlah = $this->input->post('jumlah');

      $this->form_validation->set_error_delimiters('<div id="error">', '</div>');
	$this->form_validation->set_rules('jumlah','jumlah', 'numeric|trim|required');

 		if($this->form_validation->run() == FALSE){
     $ref = $this->input->server('HTTP_REFERER', TRUE);
         echo "<script>alert('periksa kembali inputan anda');history.go(-1);</script>";
   
    }else{
      	if($jumlah > $stok)
      	{
      		 echo "<script>alert('jumlah tidak tersedia');history.go(-1);</script>";
      	}
      	else{
      			$data = array(
							'id_barang_mentah'   		=> $id_barang_mentah,
							'jumlah'  		=> $jumlah,
							'id_karyawan'  		=> $id_karyawan,
							'tanggal_permintaan'  		=> $today
						);
     
            $query = $this->M_produksi->input_data($data,'Permintaan_barang');
            if($query!== FALSE){

                 $this->session->set_flashdata("pesan", 
                 	"   <div class=\"alert alert-success alert-dismissable\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"
                                                        aria-hidden=\"true\">×
                                                </button>
                                                <strong>berhasil</strong>
                                                Request Barang Terkirim
                                            </div> <div id=\"card-alert\" class=\"card green fade in\"> 
                    ");

                 redirect('produksi/c_produksi/Permintaan_barang');

            }else{
                $ref = $this->input->server('HTTP_REFERER', TRUE);
                  redirect('produksi/c_produksi/add_barang_mentah');

            }
      	}

    		
       
    }
 	
}
/*----------------------------------------------------------------------------------------------
												Permintaan Barang
-----------------------------------------------------------------------------------------------*/

	public function Permintaan_barang()
	{
	
		
		$data['hasil'] = $this->M_produksi->tampil_username();
		$this->load->view('produksi/v_nav',$data);
		$data['hasil'] = $this->M_produksi->getRequest_barang();
		$this->load->view('produksi/v_permintaan_barang',$data);
	}
/*----------------------------------------------------------------------------------------------
											Konfirmasi Permintaan Barang
-----------------------------------------------------------------------------------------------*/

	public function konfirmasi_permintaan()
	{
	
		
		$data['hasil'] = $this->M_produksi->tampil_username();
		$this->load->view('produksi/v_nav',$data);
		$data['hasil'] = $this->M_produksi->getKonfirm_barang();
		$this->load->view('produksi/v_konfirmasi_barang_mentah',$data);
	}

	public function konfirm_barang()
  {
    $id_pengiriman_barang = $this->input->post('id_pengiriman_barang');

    if ($this->input->post('terima')) 
    {
       $this->M_produksi->permintaan_oke($id_pengiriman_barang);
    
     
       redirect('produksi/c_produksi/konfirmasi_permintaan');

    }
    else
    {
       $this->M_produksi->permintaan_batal($id_pengiriman_barang);
        redirect('produksi/c_produksi/konfirmasi_permintaan');

    } 
    
  }
/*----------------------------------------------------------------------------------------------
											Pengiriman Barang Jadi
-----------------------------------------------------------------------------------------------*/
	
	public function barang_jadi()
	{
		$data['hasil'] = $this->M_produksi->tampil_username();
		$this->load->view('produksi/v_nav', $data);
		$data['getbarangjadi'] = $this->M_produksi->getbarangjadi();
		$this->load->view('produksi/v_barang_jadi', $data);
	}

	public function pengiriman_barang_jadi()
	{
		$data['hasil'] = $this->M_produksi->tampil_username();
		$this->load->view('produksi/v_nav',$data);
		$data['getwarna'] = $this->M_produksi->getwarna();
		$this->load->view('produksi/v_pengiriman_barang_jadi',$data);
	}

	public function add_aksi_pengirimanjadi()
	{
		$kode_produk = $this->input->post('kode_produk');
		$nama_barang_jadi = $this->input->post('nama_barang_jadi');
		$warna = $this->input->post('warna');
		$jumlah = $this->input->post('jumlah');

		$data = array(
        'kode_produk' => $kode_produk,
        'nama_barang_jadi' => $nama_barang_jadi,
        'id_warna' => $warna,
        'jumlah' => $jumlah

          );
     $this->M_produksi->input_data($data,'barang_jadi');
     redirect('produksi/c_produksi/pengiriman_barang_jadi');
	}
	
	

}