<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {
	function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->helper("url");
			$this->load->library('session');
		}

	public function index()
	{
		$this->load->view('nav');
		$this->load->view('index');
		$this->load->view('footer');
	}
}
