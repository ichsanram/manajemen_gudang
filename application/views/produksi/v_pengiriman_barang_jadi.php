 <div class="wrapper">
       
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-recycle"></i>
                               Pengiriman Barang Jadi
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
            <div class="col-lg-12">
  
                            <div class="card m-t-35">
                                <div class="card-header bg-white">
                                    Input Pengiriman Barang Jadi
                                </div>
                                <div class="card-block seclect_form">
                                   <?php 
                                                 echo form_open('produksi/c_produksi/add_aksi_pengirimanjadi')  ?>
                                        <h5>Kode Produk</h5>
                                        <input name="kode_produk" id="kode_produk"  type="text" placeholder="Masukan Kode Produk" class="form-control rounded_input"/>
                                    </br>
                                        <h5>Nama Barang Jadi</h5>
                                        <input name="nama_barang_jadi" id="nama_barang_jadi"  type="text" placeholder="Masukan Nama Barang" class="form-control rounded_input" value=""/>
                                    </br>
                                   
                                        <div class="col-lg-4 input_field_sections">
                                            <div class="form-group">
                                                <select class="form-control" name="warna" id="warna" required/>
                                                    <option value="" disabled selected> --Warna-- </option>
                                                <?php foreach ($getwarna as $row) {  
                                                    echo "<option value='".$row->id_warna."'>".$row->warna."</option>";
                                                    } ?>   
                                                </select>
                                                 <?php echo form_error('warna'); ?>
                                            </div>
                                     </div>
                                    </br>
                                     
                                        <h5> Jumlah </h5>

                                            <input name="jumlah" id="jumlah"  type="text" placeholder="" class="form-control rounded_input" required />
                                      </br>
                                            <button type="submit"> Simpan</button>

                                   
                                </div>
                            </div>
                        </div>
        </div>
     </div>


<script>
  $(function() {
      $("#jumlah").keydown(function (e) {
          // Allow: backspace, delete, tab, escape, enter and .
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
               // Allow: Ctrl+A
              (e.keyCode == 65 && e.ctrlKey === true) ||
               // Allow: Ctrl+C
              (e.keyCode == 67 && e.ctrlKey === true) ||
               // Allow: Ctrl+X
              (e.keyCode == 88 && e.ctrlKey === true) ||
               // Allow: home, end, left, right
              (e.keyCode >= 35 && e.keyCode <= 39)) {
                   // let it happen, don't do anything
                   return;
          }
          // Ensure that it is a number and stop the keypress
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
              e.preventDefault();
          }
      });

  });
</script>