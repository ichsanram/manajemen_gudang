<body class="fixedMenu_left">
<div id="wrap">
  
    <div class="wrapper">
       
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-home"></i>
                                Dashboard
                            </h4>
                        </div>
                    </div>
                </div> 
            </header>
                <div class="row">
                
                    
                            <div class="col-lg-3">
                                            <div class="card m-t-20">
                                                <div class="card-header card-primary text-white">Barang Masuk</div>
                                                <div class="card-block" style="background-color:primary;">
                                                     <p>
                                                      Total Barang Masuk
                                                    </p>
                                                    <p style="position:right; font-size:25pt; ">
                                                       <?= $barang_mentah ?>
                                                    </p>
                                                </div>
                                            </div>
                    </div>

                            <div class="col-lg-3">
                                            <div class="card m-t-20">
                                                <div class="card-header card-primary text-white">Barang Masuk</div>
                                                <div class="card-block" style="background-color:primary;">
                                                     <p>
                                                      Konfirmasi Permintaan Barang mentah
                                                    </p>
                                                    <p style="position:right; font-size:25pt; ">
                                                       <?= $permintaan_barang ?>
                                                    </p>
                                                </div>
                                            </div>
                    </div>
                    <div class="col-lg-3">
                                            <div class="card m-t-20">
                                                <div class="card-header card-primary text-white">Barang Masuk</div>
                                                <div class="card-block" style="background-color:primary;">
                                                     <p>
                                                      Konfirmasi Pengiriman Barang Jadi
                                                    </p>
                                                    <p style="position:right; font-size:25pt; ">
                                                       <?= $pengiriman_barang_mentah ?>
                                                    </p>
                                                </div>
                                            </div>
                    </div>
                     <div class="col-lg-3">
                                            <div class="card m-t-20">
                                                <div class="card-header card-primary text-white">Barang Masuk</div>
                                                <div class="card-block" style="background-color:primary;">
                                                     <p>
                                                      Data Barang Mentah
                                                    </p>
                                                    <p style="position:right; font-size:25pt; ">
                                                       <?= $barang_jadi ?>
                                                    </p>
                                                </div>
                                            </div>
                    </div>

                </div>
            

        </div>
     </div>
            
</div>

