<body class="fixedMenu_left">
    
<div id="wrap">
   
    <div class="wrapper">

        <!-- /#left -->

        <div id="content" class="bg-container">
         

            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-table"></i> 
                               Konfirmsi Barang Mentah
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
    
               <div class="col-lg-12">
                      <p><?php echo $this->session->flashdata('pesan')?> </p>   
               </div>
   

              <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Data Konfirmsi Barang Mentah
                        </div>
                     
                         <div class="card-block p-t-10">
                                        <div class=" m-t-25">
                                            <table class="table table-striped table-bordered table-hover " id="sample_6">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Barang</th>
                                                    <th>Warna</th>
                                                    <th>jumlah permintaan</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                   <?php 
                                             
                                                 echo form_open('produksi/c_produksi/konfirm_barang')  ?>
                                                <tbody>
                                                    
                                                     <?php 
                                                   
                                                     $no = 1 ;
                                                     foreach($hasil as $row){?>
                                                        <tr>
                                                                <th><?php echo $no++?></th>

                                                                <th><?php echo "$row->deskripsi_barang"; ?></th>
                                                                <th><?php echo "$row->warna"; ?></th>
                                                                <th><?php echo "$row->jumlah" ;  ?></th>
                                                                <th>
                                                              <?php if ($row->status_pengiriman != null) 
                                                                {
                                                                    if($row->status_pengiriman==1)
                                                                    {
                                                                        echo "Terkirim";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo "Tidak Terkirim";
                                                                    }
                                                                }
                                                               else
                                                                {
                                                                   ?>
                                                                <input type="hidden" name="id_pengiriman_barang" value="<?php echo $row->id_pengiriman_barang ?>">
                                                                <button type="submit" value="terima" class="btn btn-success" name="terima">Terkirim</button>
                                                                <button type="submit" value="tolak" class="btn btn-danger" name="tolak">Tidak Terkirim</button>
                                                                </th>
                                                                <?php }  ?>
                                                            </tr>
                                                        </tbody>
                                                        
                                                 <?php } ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                    </div>
                </div> 
        </div>
     </div>
            
</div>

