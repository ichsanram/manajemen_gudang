 <div class="wrapper">
       
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-arrow-left"></i>
                              Permintaan Barang Mentah
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
            <div class="col-lg-12">
 
                            <div class="card m-t-35">
                                <div class="card-header bg-white">
                                    Kirim Permintaan
                                </div>
                                <div class="card-block seclect_form">
                                   <?php echo form_open('produksi/c_produksi/add_aksi_barangmentah') ?>
                                      <?php  foreach($barang_mentah as $row) { ?>
                                        <h5>Nama Barang</h5>
                                          <input name="id_barang_mentah" id="id_barang_mentah"  type="hidden" class="form-control rounded_input" value="<?php echo $row['id_barang_mentah'];?>" />
                                        <input name="deskripsi_barang" id="deskripsi_barang"  type="text" class="form-control rounded_input" value="<?php echo $row['deskripsi_barang'];?>" readonly/>
                                    </br>

                                    
                                        <h5>Warna</h5>
                                        <input name="warna" id="warna"  type="text" placeholder="" class="form-control rounded_input" value="<?php echo $row['warna'];?>" readonly/>
                                    </br>
                                
                                        <h5>Jumlah</h5>
                                       <input name="stok" id="stok"  type="text" placeholder="" class="form-control rounded_input" value="<?php echo $row['stok'];?>" readonly/>
                                    <?php }?> 
                                        <hr>
                                        <p> Input Jumlah Permintaan <p>

                                            <input name="jumlah" id="jumlah"  type="text" placeholder="" class="form-control rounded_input" required />

                                            <button type="submit"> Simpan</button>

                                    <?php echo form_close() ?>
                                </div>
                            </div>
                        </div>
        </div>
     </div>


<script>
  $(function() {
      $("#jumlah").keydown(function (e) {
          // Allow: backspace, delete, tab, escape, enter and .
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
               // Allow: Ctrl+A
              (e.keyCode == 65 && e.ctrlKey === true) ||
               // Allow: Ctrl+C
              (e.keyCode == 67 && e.ctrlKey === true) ||
               // Allow: Ctrl+X
              (e.keyCode == 88 && e.ctrlKey === true) ||
               // Allow: home, end, left, right
              (e.keyCode >= 35 && e.keyCode <= 39)) {
                   // let it happen, don't do anything
                   return;
          }
          // Ensure that it is a number and stop the keypress
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
              e.preventDefault();
          }
      });

  });
</script>