<body class="fixedMenu_left">
    
<div id="wrap">
   
    <div class="wrapper">

        <!-- /#left -->

        <div id="content" class="bg-container">
         

            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-table"></i> 
                                Barang Mentah
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
    
               <div class="col-lg-12">
                      <p><?php echo $this->session->flashdata('pesan')?> </p>   
               </div>
   

              <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Daftar Barang Mentah
                        </div>
                     
                         <div class="card-block p-t-10">
                                        <div class=" m-t-25">
                                            
                                            <table class="table table-striped table-bordered table-hover " id="sample_6">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Barang</th>
                                                    <th>Warna</th>
                                                    <th>Jumlah Permintaan</th>
                                                    <th>Status</th>
                                                    <th>Keterangan Ditolak</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                     <?php 
                                                     $no = 1 ;
                                                     foreach($hasil as $row){?>
                                                        <tr>
                                                                <th><?php echo $no++?></th>
                                                                <th><?php echo "$row->deskripsi_barang";?></th>
                                                                <th><?php echo "$row->warna";?></th>
                                                                <th><?php echo "$row->jumlah" ;?></th>
                                                                <?php
                                                                if($row->status == '0') { ?>
                                                                    <th> Ditolak </th> 
                                                                <?php } else if ($row->status == '1') { ?>
                                                        
                                                                        <th> Diterima </th>
                                                                 <?php }  else { ?>

                                                                    <th> - </th>
                                                                 
                                                                <?php } ?>
                                                                <th><?php echo "$row->keterangan_tolak" ;?></th>
                                                        </tr>
                                                        <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                    </div>
                </div> 
        </div>
     </div>
            
</div>

