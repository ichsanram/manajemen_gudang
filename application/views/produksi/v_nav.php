<!doctype html>
<html class="no-js" lang="en">


<!-- Mirrored from demo.lorvent.com/admire2_fixed_menu/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 May 2017 08:01:49 GMT -->
<head>
    <meta charset="UTF-8">
    <title>Manajemen Gudang</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/logo1.ico"/>

    <!-- jQuery Library -->
    <!-- <script type="text/javascript" src="<?= base_url("assets/js/plugins/jquery-1.11.2.min.js")?>"></script> -->

    <!--global styles-->
    <link href="<?= base_url("assets/css/components.css")?>" rel="stylesheet" type="text/css" media="screen,projection">
    <link href="<?= base_url("assets/css/custom.css")?>" rel="stylesheet" type="text/css" media="screen,projection">
    <link href="<?= base_url("assets/vendors/chartist/css/chartist.min.css")?>" rel="stylesheet" type="text/css" media="screen,projection">
    <link href="<?= base_url("assets/vendors/circliful/css/jquery.circliful.css")?>" rel="stylesheet" type="text/css" media="screen,projection">
    <link href="<?= base_url("assets/css/pages/index.css")?>" rel="stylesheet" type="text/css" media="screen,projection">
    <link href="<?= base_url("assets/css/components.css")?>" rel="stylesheet" type="text/css" media="screen,projection">
     <link href="<?= base_url("assets/css/pages/tables.css")?>" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="#" id="skin_change" />

    <!--JAVASCRIPT-->
    <!-- /#wrap -->
    <!-- global scripts-->
    


</head>
<body>
 <div id="left" class="fixed">
            <div class="menu_scroll left_scrolled">
                <div class="left_media">
                    <div class="media user-media">
                        <div class="user-media-toggleHover">
                            <span class="fa fa-user"></span>
                        </div>
                        <div class="user-wrapper">
                            <a class="user-link" href="#">
                                <img class="media-object img-thumbnail user-img rounded-circle admin_img3" alt="User Picture"
                                     src="<?= base_url("assets/img/vcoll.jpg")?>">
                                <p class="user-info menu_hide">Adm Produksi</p>
                            </a>
                        </div>
                    </div>
                    <hr/>
                </div>
                <ul id="menu">
                    <li>
                        <a href="<?php echo site_url('produksi/c_produksi/index'); ?>">
                            <i class="fa fa-home"></i>
                            <span class="link-title menu_hide">&nbsp;Dashboard </span>
                        </a>
                    </li>
                   
                    <li>
                        <a href="<?php echo site_url('produksi/c_produksi/barang_mentah'); ?>">
                            <i class="fa fa-table"></i>
                            <span class="link-title menu_hide">&nbsp;Data Barang Mentah</span>
                        </a>
                    </li>
            
                    <li>
                        <a href="<?php echo site_url('produksi/c_produksi/permintaan_barang'); ?>">
                            <i class="fa fa-arrow-left"></i>
                            <span class="link-title menu_hide">&nbsp;Permintaan Barang </span>
                        </a>
                    </li>
                    <li class="dropdown_menu">
                        <a href="#">
                            <i class="fa fa-th"></i>
                            <span class="link-title menu_hide">&nbsp; Konfirmasi Barang</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                             <li>

                                <a href="<?php echo site_url('produksi/c_produksi/konfirmasi_permintaan'); ?>">
                                    <i class="fa fa-arrow-right"></i>
                                    <span class="link-title menu_hide">&nbsp;Pengiriman Barang Mentah </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    <li>
                        <a href="<?php echo site_url('produksi/c_produksi/barang_jadi'); ?>">
                            <i class="fa fa-recycle"></i>
                            <span class="link-title menu_hide">&nbsp;Pengiriman Barang Jadi </span>
                        </a>
                    </li>
                   
            </div>
        </div>


<body class="fixedMenu_left">
<div id="wrap">
    <div id="top">
        <nav class="navbar navbar-static-top">
            <div class="container-fluid m-0">
                <a class="navbar-brand float-left" href="index-2.html">
                    <h4>  PT. V COLLECTION </h4>
                </a>
                
                <div class="topnav dropdown-menu-right float-right">
                   
                    <div class="btn-group">
                        <div class="user-settings no-bg">
                            <button type="button" class="btn btn-default no-bg micheal_btn" data-toggle="dropdown">
                               <?php foreach($hasil as $row) {
                                echo"  <strong>$row->username</strong>";
                                }?>
                             
                                <span class="fa fa-sort-down white_bg"></span>
                            </button>
                            <div class="dropdown-menu admire_admin">
                                <a class="dropdown-item title" href="#">
                                <?php foreach($hasil as $row) {
                                  echo"$row->username</a>";
                                  }  ?>
                                
                                <a class="dropdown-item" href="<?php echo site_url('produksi/c_produksi/logout')?>"><i class="fa fa-sign-out"></i>
                                    Log Out</a>
                            </div>
                        </div>
                    </div>

                </div>
                
            </div>
        </nav>
    </div>
<script type="text/javascript" src="<?= base_url("assets/js/jquery.min.js")?>"></script>
<script type="text/javascript" src="<?= base_url("assets/js/components.js")?>"></script>
<script type="text/javascript" src="<?= base_url("assets/js/custom.js")?>"></script>

    <!--end of global scripts-->
    <!--plugin scripts-->
    <script type="text/javascript" src="<?= base_url("assets/vendors/select2/js/select2.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/datatables/js/jquery.dataTables.min.js")?>"></script> 
    <script type="text/javascript" src="<?= base_url("assets/js/pluginjs/dataTables.tableTools.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/datatables/js/dataTables.colReorder.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/datatables/js/dataTables.bootstrap.min.js")?>"></script>
    
      <script type="text/javascript" src="<?= base_url("assets/vendors/datatables/js/dataTables.buttons.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/js/pluginjs/jquery.dataTables.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/datatables/js/dataTables.responsive.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/datatables/js/dataTables.rowReorder.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/datatables/js/buttons.colVis.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/datatables/js/buttons.html5.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/datatables/js/buttons.bootstrap.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/datatables/js/buttons.print.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/datatables/js/dataTables.scroller.min.js")?>"></script>


    <!-- end of plugin scripts -->
    <!--Page level scripts-->
    <script type="text/javascript" src="<?= base_url("assets/js/pages/datatable.js")?>"></script>
    <!-- end of global scripts-->

</html>