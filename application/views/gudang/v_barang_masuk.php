<body class="fixedMenu_left">
<div id="wrap">
    <div class="wrapper">
       
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-table"></i>
                                Barang Mentah
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
          <div>
              <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Daftar Barang Mentah
                        </div>
                         <div class="card-block p-t-10">
                             <a class="btn btn-primary" href="<?= site_url('gudang/c_gudang/add_barang_masuk');?>">Tambah Barang</a>
                                        <div class=" m-t-25">
                                            <table class="table table-striped table-bordered table-hover " id="sample_6">
                                                <thead>
                                                <tr>
                                                    <th> No </th>
                                                    <th>Nama Barang</th>
                                                    <th>Nama Suplier</th>
                                                    <th>Jumlah</th>
                                                    <th>Warna</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                     <?php 
                                                    $no = 1 ;
                                                    foreach($hasil as $row){?>
                                                        <tr>
                                                                <td><?php echo $no++?></td>
                                                                <td><?php echo $row['deskripsi_barang'] ?></td>
                                                                <td><?php echo $row['nama_supplier'] ?></td>
                                                                <td><?php echo $row['jumlah'] ?></td>
                                                                <td><?php echo $row['warna'] ?></td>
                                                        </tr>
                                                        <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div> 
        </div>
     </div>
            
</div>

