
<body class="fixedMenu_left">
<div id="wrap">
    <div class="wrapper">
       
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-table"></i>
                                Data Pengiriman Barang Jadi
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
          <div>
              <div class="card m-t-35">

                        <div class="card-header bg-white">
                            Barang Jadi
                        </div>
                         <div class="card-block p-t-10">

                                        <div class=" m-t-25">
                                            <table class="table table-striped table-bordered table-hover " id="sample_6">
                                                <thead>
                                                    <th>No</th>
                                                    <th>kode Produk</th>
                                                    <th>Nama Barang Jadi</th>
                                                    <th>Warna</th>
                                                    <th>Jumlah</th>
                                                    <th>Status Pengiriman</th>
                                                </thead>

                                                <tbody>
                                                     <?php 
                                                 echo form_open('gudang/c_gudang/aksi_konfirm_barangjadi')  ?>
                                                 <?php 
                                                     $no = 1 ;
                                                     foreach($hasil as $row){?>
                                                        <tr>
                                                                <th><?php echo $no++?></th>

                                                                <th><?php echo "$row->kode_produk"; ?></th>
                                                                <th><?php echo "$row->nama_barang_jadi"; ?></th>
                                                                <th><?php echo "$row->warna"; ?></th>
                                                                <th><?php echo "$row->jumlah" ;  ?></th>
                                                                <th>
                                                              <?php if ($row->status != null) 
                                                                {
                                                                    if($row->status==1)
                                                                    {
                                                                        echo "Diterima";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo "Ditolak";
                                                                    }
                                                                } 
                                                               else

                                                                {
                                                                   ?>
                                                               <a class="btn btn-primary" href="<?= site_url('gudang/c_gudang/add_barang_masuk');?>">Tambah Barang</a>
                                                                </th>
                                                                <?php }  ?>
                                                               
                                                        </tr>
                                                <?php } ?>
                                                 <?php echo form_close() ?>
                                                </tbody>
                                                 
                                            </table>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div> 
        </div>
     </div>
            
</div>

