<body class="fixedMenu_left">
<div id="wrap">
    <div class="wrapper">
       
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-table"></i>
                                Barang Mentah
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
          <div>
              <div class="card m-t-35">

                        <div class="card-header bg-white">
                            Daftar Barang Mentah
                        </div>
                         <div class="card-block p-t-10">

                                        <div class=" m-t-25">
                                            <table class="table table-striped table-bordered table-hover " id="sample_6">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Barang</th>
                                                    <th>warna</th>
                                                    <th>stok</th>
                                                  
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                     <?php 
                                                    $no = 1 ;
                                                    foreach($hasil as $row){?>
                                                        <tr>
                                                                <th><?php echo $no++?></th>
                                                                <th><?php echo "$row->deskripsi_barang"; ?></th>
                                                                <th><?php echo "$row->warna"; ?></th>
                                                                <th><?php echo "$row->stok" ;  ?></th>
                                                               
                                                                <th>
                                                                    
                                                                    <a class="btn btn-danger" href="<?php echo site_url('gudang/c_gudang/hapus_barang_mentah/'.$row->id_barang_mentah);?>">Delete</a>

                                                                </th>
                                                        </tr>
                                                        <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div> 
        </div>
     </div>
            
</div>

