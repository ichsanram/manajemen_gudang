<body class="fixedMenu_left">
<div id="wrap">
  
    <div class="wrapper">
       
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-home"></i>
                                Warna
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
                <div class="col-lg-12">
                      <p><?php echo $this->session->flashdata('pesan')?> </p>   
               </div>
   
             <div>
              <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Daftar Barang Mentah
                        </div>
                     
                         <div class="card-block p-t-10">
                          <a class="btn btn-primary" href="<?= site_url('gudang/c_gudang/add_warna');?>">Tambah Warna</a>
                                        <div class=" m-t-25">
                                            <table class="table table-striped table-bordered table-hover " id="sample_6">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Warna Barang</th>
                                                    <th>Action</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                     <?php 
                                                    $no = 1 ;
                                                    foreach($hasil as $row){?>
                                                        <tr>
                                                                <th><?php echo $no++?></th>
                                                                <th><?php echo "$row->warna"; ?></th>
                                                                <th>
                                                                    <a class="btn btn-success" href="<?php echo site_url('gudang/c_gudang/edit_warna/'.$row->id_warna);?>">Edit</a>
                                                                    <a class="btn btn-danger" href="<?php echo site_url('gudang/c_gudang/hapus_warna/'.$row->id_warna);?>">Delete</a>

                                                                </th>
                                                        </tr>
                                                        <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                    </div>
           
        </div>
     </div>
            
</div>

