

<body class="fixedMenu_left">
<div id="wrap">
   
    <div class="wrapper">
       
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-table"></i>
                               Permintaan Barang Mentah
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
            <div>
              <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Daftar Permintaan Barang Mentah
                        </div>
                     
                         <div class="card-block p-t-10">
                         
                                        <div class=" m-t-25">
                                            <table class="table table-striped table-bordered table-hover " id="sample_6">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Barang</th>
                                                    <th>Nama Karyawan</th>
                                                    <th>Jumlah</th>
                                                    <th>Tgl Permintaan</th>
                                                    <th>Status Permintaan</th>
                                                    <th>Tgl Pengiriman</th>
                                                    <th>Status Pengiriman</th>
                                                </tr>
                                                </thead>
                                           
                                                <tbody>
                                                 <?php 
                                                    $no = 1 ;
                                                    foreach($hasil as $row){?>
                                                        <tr>
                                                                <td><?php echo $no++?></td>
                                                                <td><?php echo $row['deskripsi_barang'] ?></td>
                                                                <td><?php echo $row['username'] ?></td>
                                                                <td><?php echo $row['jumlah'] ?></td>
                                                                <td><?php echo $row['tanggal_permintaan'] ?></td>
                                                                <td>
                                                                <?php if ($row['status']!= null) 
                                                                {
                                                                    if($row['status']==1)
                                                                    {
                                                                        echo "Diterima";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo "Ditolak";
                                                                    }
                                                                }
                                                               else
                                                                {
                                                                   ?>
                                                                    <?php 
                                                                    // echo  $this->db->last_query();
                                                         echo form_open('gudang/c_gudang/konfirm_barang')  ?>
                                                                <!-- <a class="btn btn-primary" href=" //site_url('gudang/c_gudang/add_barang_masuk');?>">
                                                              Konfirmasi Permintaan </a>-->

                                                            <input type="hidden" name="id_barang_mentah" value="<?php echo $row ['id_barang_mentah'] ;?>" >
                                                            <input type="hidden" name="id_permintaan_barang" value="<?php echo $row ['id_permintaan_barang'] ?>">
                                                            <input type="hidden" name="stok" value="<?php echo $row ['stok'] ?>">
                                                            <input type="hidden" name="jumlah" value="<?php echo $row['jumlah'];?>">
                                                            <input type="submit" value="terima" class="btn btn-success" name="terima"></input>  
                                                            <a class="btn btn-primary" href="<?= site_url('gudang/c_gudang/formkonfirm_permintaan/'.$row['id_permintaan_barang']);?>">Tolak</a>

                                                              <?php echo form_close() ?>
                                                                   <?php }  ?>
                                                                </td>
                                                             
                                                                <td><?php echo $row['tanggal_terkirim'] ?></td>
                                                                <td>
                                                                <?php if ($row['status_pengiriman']!== null)
                                                                {
                                                                    if($row['status_pengiriman']==1)
                                                                    {
                                                                        echo "Terkirim";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo "Tidak Terkirim";
                                                                    }
                                                                }
                                                                 ?>
                                                                    
                                                                </td>
                                                        </tr>
                                                            <?php } ?>
                                                          </tbody>

                                                     
                                              
                                            </table>
                                        </div>
                                    </div>
                                </div>

                    </div>
                </div> 
        </div>
     </div>
            
</div>

