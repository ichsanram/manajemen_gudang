<body class="fixedMenu_left">
<div id="wrap">
   
    <div class="wrapper">
       
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-user"></i>
                               Data Barang Masuk
                            </h4>
                        </div>
                    </div>
                </div>
            </header>

                 <div class="card-content red-text">
                        <p></p>
                      </div>
                    
            <div class="col-lg-12">
                            <div class="card m-t-35">
                                <div class="card-header bg-white">
                                    Tambah Barang Masuk
                                </div>
                                <div class="card-block seclect_form">
                                    <form class="form-horizontal" action="<?= site_url('gudang/c_gudang/add_aksi_barang_masuk');?>" method="post" >
                                     <div class="col-lg-4 input_field_sections">
                                            <div class="form-group">
                                                <select class="form-control" name="deskripsi_barang" id="deskripsi_barang" required/>
                                                    <option value="" disabled selected>  --Nama barang--</option>
                                                <?php foreach ($getnamabarang as $row) {  
                                                      echo "<option value='".$row['id_jenis_barang']."'>".$row['deskripsi_barang']."</option>";
                                                                                                     } ?>  
                                                </select>
                                                 <?php echo form_error('nama_supplier'); ?>
                                            </div>
                                     </div>
                                     <div class="col-lg-4 input_field_sections">
                                            <div class="form-group">
                                                <select class="form-control" name="nama_supplier" id="nama_supplier" required/>
                                                    <option value="" disabled selected>  --Nama Supplier--</option>
                                                <?php foreach ($getsupplier as $row) {  
                                                    echo "<option value='".$row->id_supplier."'>".$row->nama_supplier."</option>";
                                                    } ?>  
                                                </select>
                                                 <?php echo form_error('nama_supplier'); ?>
                                            </div>
                                     </div>
                                     <div>
                                        <h5> Jumlah </h5>
                                        <input name="jumlah" id="jumlah"  type="number" placeholder="jumlah barang" class="form-control rounded_input" required/>
                                         <?php echo form_error('jumlah'); ?>
                                     </div>
                                     <div class="col-lg-4 input_field_sections">
                                            <div class="form-group">
                                                <select class="form-control" name="warna" id="warna" required/>
                                                    <option value="" disabled selected> --Warna-- </option>
                                                <?php foreach ($getwarna as $row) {  
                                                    echo "<option value='".$row->id_warna."'>".$row->warna."</option>";
                                                    } ?>   
                                                </select>
                                                 <?php echo form_error('warna'); ?>
                                            </div>
                                     </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Simpan</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
        </div>
     </div>
            
</div>

