<!doctype html>
<html class="no-js" lang="en">


<!-- Mirrored from demo.lorvent.com/admire2_fixed_menu/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 May 2017 08:01:49 GMT -->
<head>
    <meta charset="UTF-8">
    <title>Dashboard-2 | Admire</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/logo1.ico"/>

    <!-- jQuery Library -->
    <script type="text/javascript" src="<?= base_url("assets/js/plugins/jquery-1.11.2.min.js")?>"></script>

    <!--global styles-->
    <link href="<?= base_url("assets/css/components.css")?>" rel="stylesheet" type="text/css" media="screen,projection">
    <link href="<?= base_url("assets/css/custom.css")?>" rel="stylesheet" type="text/css" media="screen,projection">
    <link href="<?= base_url("assets/vendors/chartist/css/chartist.min.css")?>" rel="stylesheet" type="text/css" media="screen,projection">
    <link href="<?= base_url("assets/vendors/circliful/css/jquery.circliful.css")?>" rel="stylesheet" type="text/css" media="screen,projection">
    <link href="<?= base_url("assets/css/pages/index.css")?>" rel="stylesheet" type="text/css" media="screen,projection">
    <link href="<?= base_url("assets/css/components.css")?>" rel="stylesheet" type="text/css" media="screen,projection">
    <link type="text/css" rel="stylesheet" href="#" id="skin_change" />

    <!--JAVASCRIPT-->
    <!-- /#wrap -->
    <!-- global scripts-->
    <script type="text/javascript" src="<?= base_url("assets/js/components.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/js/custom.js")?>"></script>
    <!--end of global scripts-->
    <!--  plugin scripts -->
    <script type="text/javascript" src="<?= base_url("assets/vendors/countUp.js/js/countUp.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/flip/js/jquery.flip.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/js/pluginjs/jquery.sparkline.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/chartist/js/chartist.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/js/pluginjs/chartist-tooltip.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/swiper/js/swiper.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/circliful/js/jquery.circliful.min.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/flotchart/js/jquery.flot.js")?>"></script>
    <script type="text/javascript" src="<?= base_url("assets/vendors/flotchart/js/jquery.flot.resize.js")?>"></script>
    <!--end of plugin scripts-->
    <script type="text/javascript" src="<?= base_url("assets/js/pages/index.js")?>"></script>


</head>