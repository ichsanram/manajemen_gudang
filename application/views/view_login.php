<!DOCTYPE html>
<html>

<!-- Mirrored from demo.lorvent.com/admire2_fixed_menu/login2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 May 2017 08:05:01 GMT -->
<head>
    <title>Manajemen Gudang</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!--Global styles -->
    <link type="text/css" rel="stylesheet" href="<?= base_url("assets/css/components.css")?>" />
    <link type="text/css" rel="stylesheet" href="<?= base_url("assets/css/custom.css")?>" />
    <!--End of Global styles -->
    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="<?= base_url("assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css")?>"/>
    <link type="text/css" rel="stylesheet" href="<?= base_url("assets/vendors/wow/css/animate.css")?>"/>
    <!--End of Plugin styles-->
    <link type="text/css" rel="stylesheet" href="<?= base_url("assets/css/pages/login2.css");?>"/>
</head>
<body class="login_background">
<div class="preloader" style=" position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 100000;
  backface-visibility: hidden;
  background: #ffffff;">
    <div class="preloader_img" style="width: 200px;
  height: 200px;
  position: absolute;
  left: 48%;
  top: 48%;

  background-position: center;
z-index: 999999">
        <!--<img src="<?php echo base_url();?>img/loader.gif" style=" width: 40px;" alt="loading...">-->
        
    </div>
</div>
<div class="container wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">
    <div class="row">
        <div class="col-xl-4 push-xl-4 col-lg-6 push-lg-3 col-md-8 push-md-2 col-sm-8 push-sm-2 col-10 push-1">
            <div class="row">
                <div class="col-lg-10 push-lg-1 col-md-10 push-md-1 col-sm-12 login_image login_section login_section_top">
                    <div class="login_logo login_border_radius1">
                        
                    </div>
                    <div class="row m-t-20">
                        <div class="col-12">
                            <a class="text-success m-r-20 font_18">LOG IN</a>
                        </div>
                    </div>
                    <div class="m-t-15">
                        
                            <div class="form-group">
                                <?php echo form_open('login/cek_login') ?>
                                <label for="username" class="col-form-label text-white"> Username</label>
                                <input type="text" class="form-control b_r_20" id="user" name="user" placeholder="username">
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-form-label text-white">Password</label>
                                <input type="password" class="form-control b_r_20" id="pass" name="pass" placeholder="Password">
                            </div>
                            <div class="row m-t-15">
                            
                            </div>
                            <div class="text-center login_bottom">
                                <button type="submit" name="login" value="login" class="btn btn-mint btn-block b_r_20 m-t-10 m-r-20">LOG IN</button>
                            </div>
                              <?php echo form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script type="text/javascript" src="<?= base_url("assets/js/jquery.min.js");?>"></script>
<script type="text/javascript" src="<?= base_url("assets/js/tether.min.js");?>"></script>
<script type="text/javascript" src="<?= base_url("assets/js/bootstrap.min.js");?>"></script>
<!-- end of global js-->
<!--Plugin js-->
<script type="text/javascript" src="<?= base_url("assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js");?>"></script>
<script type="text/javascript" src="<?= base_url("assets/vendors/wow/js/wow.min.js");?>"></script>
<script type="text/javascript" src="<?= base_url("assets/vendors/jquery.backstretch/js/jquery.backstretch.js");?>"></script>
<!--End of plugin js-->
<script type="text/javascript" src="<?= base_url("assets/js/pages/login2.js");?>"></script>

</body>


<!-- Mirrored from demo.lorvent.com/admire2_fixed_menu/login2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 May 2017 08:05:06 GMT -->
</html>