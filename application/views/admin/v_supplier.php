<body class="fixedMenu_left">
<div id="wrap">
  
    <div class="wrapper">
       
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-truck"></i>
                                Supplier
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
                <div class="col-lg-12">
                      <p><?php echo $this->session->flashdata('pesan')?> </p>   
               </div>
   
             <div>
              <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Daftar Nama Supplier
                        </div>
                     
                         <div class="card-block p-t-10">
                          <a class="btn btn-primary" href="<?= site_url('admin/c_index/add_supplier');?>">Tambah Supplier</a>
                                        <div class=" m-t-25">
                                            <table class="table table-striped table-bordered table-hover " id="sample_6">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Supplier</th>
                                                    <th>Action</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                     <?php 
                                                    $no = 1 ;
                                                    foreach($hasil as $row){?>
                                                        <tr>
                                                                <th><?php echo $no++?></th>
                                                                <th><?php echo "$row->nama_supplier"; ?></th>
                                                                 <th>
                                                                    <a class="btn btn-success" href="<?php echo site_url('admin/c_index/edit_supplier/'.$row->id_supplier);?>">Edit</a>
                                                                    <a class="btn btn-danger" href="<?php echo site_url('admin/c_index/hapus_supplier/'.$row->id_supplier);?>">Delete</a>

                                                                </th>
                                                        </tr>
                                                        <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                    </div>
           
        </div>
     </div>
            
</div>

