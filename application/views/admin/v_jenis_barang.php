

<body class="fixedMenu_left">
<div id="wrap">
   
    <div class="wrapper">
       
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar"> 
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-table"></i>
                             Jenis Barang
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
            <div class="col-lg-12">
                      <p><?php echo $this->session->flashdata('pesan')?> </p>   
               </div>
            <div>
              <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Daftar  Jenis Barang
                        </div>
                     
                         <div class="card-block p-t-10">
                               <a class="btn btn-primary" href="<?= site_url('admin/c_index/add_jenis_barang');?>">Tambah Nama Barang</a>

                                        <div class=" m-t-25">
                                                            
                                            <table class="table table-striped table-bordered table-hover " id="sample_6">
                                                <thead>
                                                <tr>
                                                      <th> No </th>
                                                    <th>id jenis barang</th>
                                                    <th>deskripsi barang</th>
                                                  
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                  <tbody>
                                                     <?php 
                                                    $no = 1 ;
                                                    foreach($hasil as $row){?>
                                                        <tr>
                                                                <td><?php echo $no++?></td>
                                                                <td><?php echo $row['id_jenis_barang'] ?></td>
                                                                <td><?php echo $row['deskripsi_barang'] ?></td>

                                                                <td>
                                                                   <a class="btn btn-success" href="<?php echo site_url('admin/c_index/edit_jenis_barang/'.$row['id_jenis_barang']);?>">Edit</a>
                                                                   <a class="btn btn-danger" href="<?php echo site_url('admin/c_index/hapus_jenis_barang/'.$row['id_jenis_barang']);?>">Delete</a>

                                                                </td>
                                                        </tr>
                                                        <?php } ?>
                                                </tbody>
                                  
                                              
                                            </table>
                                        </div>
                                    </div>
                                </div>

                    </div>
                </div> 
        </div>
     </div>
            
</div>



