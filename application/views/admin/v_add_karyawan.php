<body class="fixedMenu_left">
<div id="wrap">
  
    <div class="wrapper">
       
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-user"></i>
                               Karyawan
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
            <div class="col-lg-12">
                            <div class="card m-t-35">
                                <div class="card-header bg-white">
                                    Tambah Karyawan 
                                </div>
                                <div class="card-block seclect_form">
                                    <form class="form-horizontal" action="<?= site_url('admin/c_index/add_aksi_karyawan');?>" method="post" >
                                        <h5>Username</h5>
                                        <input name="username" id="username" type="text" placeholder="username" class="form-control rounded_input"/>
                                     <div>
                                        <h5>Password</h5>
                                        <input name="password" id="password"  type="password" placeholder="password" class="form-control rounded_input"/>
                                     </div>
                                       <div class="col-lg-4 input_field_sections">
                                            <h5>Status</h5>
                                            <div class="form-group">
                                                <select class="form-control" name="status">
                                                    <option value="admin">Admin</option>
                                                    <option value="produksi">Produksi</option>
                                                    <option value="gudang">Gudang</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Simpan</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
        </div>
     </div>
            
</div>

