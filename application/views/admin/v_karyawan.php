

<body class="fixedMenu_left">
<div id="wrap">
   
    <div class="wrapper">
       
        <!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <h4 class="m-t-5">
                                <i class="fa fa-table"></i>
                             Karyawan
                            </h4>
                        </div>
                    </div>
                </div>
            </header>
            <div>
             <div class="m-t-35">
                         
         
                        <div class="card-header bg-white">
                            Daftar Karyawan
                        </div>
                     
                         <div class="card-block p-t-10">
                            <a class="btn btn-primary" href="<?= site_url('admin/c_index/add_karyawan');?>">Tambah Karyawan</a>
                                        <div class=" m-t-25">
                                            <table class="table table-striped table-bordered table-hover " id="sample_6">
                                                <thead>
                                                <tr>
                                                      <th> No </th>
                                            <th>Nama</th>
                                            <th>Password</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                                </tr>
                                                </thead>
                                           
                                                <tbody>
                                                 <?php 
                                                      $no = 1 ;
                                        foreach($hasil as $row){?>
                                                        <tr>
                                                              <td><?php echo $no++?></td>
                                                                <td><?php echo $row['username'] ?></td>
                                                                <td><?php echo $row['password'] ?></td>
                                                                <td><?php echo $row['status'] ?></td>
                                                                  <td><a class="btn btn-success" href="<?php echo site_url('admin/c_index/edit_karyawan/'.$row['id_karyawan']);?>">Edit</a>
                                                   <a class="btn btn-danger" href="<?php echo site_url('admin/c_index/hapus_karyawan/'.$row['id_karyawan']);?>">Delete</a>

                                                   </td>


                                                        </tr>
                                                            <?php } ?>
                                                          </tbody>

                                                     
                                              
                                            </table>
                                        </div>
                                    </div>
                                </div>

                    </div>
                </div> 
        </div>
     </div>
            
</div>

    