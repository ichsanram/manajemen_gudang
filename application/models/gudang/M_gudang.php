     <?php
class M_gudang extends CI_Model{

    
        function tampil_username(){

          $this->db->select("*");
          $this->db->from("karyawan");
          $karyawan=$_SESSION['id_karyawan']; 
          $this->db->where('id_karyawan',$karyawan);
          $query=$this->db->get();
          return $query->result();
        }

    public function input_data($data,$table)
                        {                           
                                $this->db->insert($table,$data);
                            
                        }         

    public function update_data($where,$data,$table)
                        {
                                $this->db->where($where);
                                $this->db->update($table,$data);
                        }
    public function hapus_data($where,$table){
                                $this->db->where($where);
                                $this->db->delete($table);
                        }
    public function edit_data($where,$table)
                        {
                                return $this->db->get_where($table,$where);
                        } 
    // public function permintaan_barang($where)
    // {
    //   $this->db->query(" UPDATE permintaan_barang set status= 1  WHERE id_permintaan_barang= '$where' ");
    //   $this->db->input('pengiriman_barang_mentah', $where);
    // }
        
  public function getwarna()
  						        {
            						      	$data = $this->db->get('warna_barang');
            						      	return $data->result(); 
      				        }
  public function getsupplier()
              {
                    $data = $this->db->get('supplier_barang');
                    return $data->result();
              }   
  public function jenis_barang(){
                                $data = $this->db->get('jenis_barang');
                                return $data->result_array();
                      }

  public function cek_barang_mentah()
                      {

                                $cek = $this->db->query("SELECT * FROM barang_mentah");
                                return $cek->result();
                      }

  public function update_barang_mentah($jumlah,$deskripsi_barang,$warna)
                      {
                                $this->db->query("UPDATE barang_mentah set barang_mentah.stok= barang_mentah.stok + ".$jumlah." WHERE barang_mentah.id_jenis_barang= '$deskripsi_barang' AND barang_mentah.id_warna= '$warna' ");
                      }
   public function getbarangmentah()
                      {
                                $this->db->select('*');
                                $this->db->join('jenis_barang','jenis_barang.id_jenis_barang = barang_mentah.id_jenis_barang');
                                $this->db->join('warna_barang','warna_barang.id_warna = barang_mentah.id_warna');
                                $data = $this->db->get('barang_mentah');
                                return $data->result();
                      }
    public function getbarangmasuk()
                      {
                                $this->db->select('*');
                                $this->db->join('jenis_barang','jenis_barang.id_jenis_barang = barang_masuk.id_jenis_barang');
                                $this->db->join('warna_barang','warna_barang.id_warna = barang_masuk.id_warna');
                                $this->db->join('supplier_barang','supplier_barang.id_supplier = barang_masuk.id_supplier');
                                $this->db->order_by('id_barang_masuk','DESC');
                                $data = $this->db->get('barang_masuk');
                                return $data->result_array();
                      }
    public function getpermintaanbarang()
                        {
                                $this->db->select('karyawan.username,barang_mentah.*,permintaan_barang.*,jenis_barang.*,pengiriman_barang_mentah.status_pengiriman,pengiriman_barang_mentah.tanggal_terkirim');
                                $this->db->join('barang_mentah','barang_mentah.id_barang_mentah = permintaan_barang.id_barang_mentah');
                                $this->db->join('jenis_barang','jenis_barang.id_jenis_barang = barang_mentah.id_jenis_barang');
                                $this->db->join('karyawan','karyawan.id_karyawan = permintaan_barang.id_karyawan');
                                $this->db->join('pengiriman_barang_mentah','pengiriman_barang_mentah.id_permintaan_barang = permintaan_barang.id_permintaan_barang','left');
                                 $this->db->order_by('id_permintaan_barang','DESC');
                                $data = $this->db->get('permintaan_barang');
                                return $data;
                        }
    public function permintaan_barang($where)
                      {
                                $this->db->select('*');
                                $this->db->join('barang_mentah','barang_mentah.id_barang_mentah = permintaan_barang.id_barang_mentah');
                                $this->db->join('jenis_barang','jenis_barang.id_jenis_barang = barang_mentah.id_jenis_barang');
                                $this->db->join('karyawan','karyawan.id_karyawan = permintaan_barang.id_karyawan');
                                $this->db->where($where);
                                $data = $this->db->get('permintaan_barang');
                                return $data;
                      }
    public function getbarangjadi()
                        {
                                $this->db->select('*');
                                $this->db->join('warna_barang','warna_barang.id_warna = barang_jadi.id_warna');
                                $data = $this->db->get('barang_jadi');
                                return $data;
                        }
    public function konfirmasi_oke ($id_barang_jadi)
                        {
                              
                               $this->db->query("UPDATE barang_jadi set status = 1  where id_barang_jadi = $id_barang_jadi ");
                               
                         
                        }
     public function konfirmasi_batal ($id_barang_jadi)
                        {
                              
                               $this->db->query("UPDATE barang_jadi set status = 0  where id_barang_jadi = $id_barang_jadi ");
                               
                         
                        }

    public function permintaan_oke ($jlh,$id_barang_mentah,$id_permintaan_barang,$stok)
                        {
                               $new_stok = $stok-$jlh ;
                               $this->db->query("UPDATE permintaan_barang set status = 1  where id_permintaan_barang = $id_permintaan_barang ");
                               $this->db->query("UPDATE barang_mentah set barang_mentah.stok= ".$new_stok." WHERE id_barang_mentah = $id_barang_mentah");
                         
                        }
    public function permintaan_batal($id_permintaan_barang)
                        {
                               $this->db->query("UPDATE permintaan_barang set status = 0 where id_permintaan_barang = $id_permintaan_barang");
                        }


   //UNTUK MERIKSA NAMA warna DAH ADA ATAU BELUM
    public function valid_nameWarna($nama_warna)
    {
        $query = $this->db->get_where('warna_barang', array('warna' => $nama_warna));
        if ($query->num_rows() > 0)
          {
              return TRUE;
          }
        else
          {
              return FALSE;
          }
        echo $this->db->last_query();
    }

    public function valid_nameSupplier($nama_supplier)
    {
        $query = $this->db->get_where('supplier_barang', array('nama_supplier' => $nama_supplier));
        if ($query->num_rows() > 0)
          {
              return TRUE;
          }
        else
          {
              return FALSE;
          }
        echo $this->db->last_query();
    }

    public function valid_nameBarang($nama_warna)
    {
        $query = $this->db->get_where('barang_masuk', array('deskripsi_barang' => $deskripsi_barang));
        if ($query->num_rows() > 0)
          {
              return TRUE;
          }
        else
          {
              return FALSE;
          }
        echo $this->db->last_query();
    }



  }   
?>