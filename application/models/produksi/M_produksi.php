 <?php
class M_produksi extends CI_Model{
        

      public  function tampil_username()
                        {
                                $this->db->select("*");
                                $this->db->from("karyawan");
                                $karyawan=$_SESSION['id_karyawan'];
                                $this->db->where('id_karyawan',$karyawan);
                                $query=$this->db->get(); 
                                return $query->result();
                        }

      public function input_data($data,$table)
    				          	{                           
                                $this->db->insert($table,$data);
                        }   

    	public function update_data($where,$data,$table)
                        {
                                $this->db->where($where);
                                $this->db->update($table,$data);
                        } 

    	public function hapus_data($where,$table){
    	                          $this->db->where($where);
                                $this->db->delete($table);
                        }

    	public function edit_data($where,$table)
                        {
                                return $this->db->get_where($table,$where);
                        }

      public function getwarna()
                        {
                               $data = $this->db->get('warna_barang');
                               return $data->result();
                        }    

      public function getbarangmentah()
  						          {
						      	
                      	        $this->db->select('warna_barang.*,jenis_barang.*,barang_mentah.*');
                      	        $this->db->join('jenis_barang','jenis_barang.id_jenis_barang = barang_mentah.id_jenis_barang');
                      	        $this->db->join('warna_barang','warna_barang.id_warna = barang_mentah.id_warna');
                      	        $data = $this->db->get('barang_mentah');
                      			    return $data->result();
						            }

      public function getbarangjadi()
                        {
                                $this->db->select('*');
                                $this->db->join('warna_barang','warna_barang.id_warna = barang_jadi.id_warna');
                                $data = $this->db->get('barang_jadi');
                                return $data->result();
                        }

		  public function getPermintaan_barang()
  						          {
						      	
                      	        $this->db->select('barang_mentah.*, jenis_barang.deskripsi_barang');
                      	        $this->db->join('jenis_barang','jenis_barang.id_jenis_barang = barang_mentah.id_jenis_barang');
                      	        $data = $this->db->get('barang_mentah');
                      			    return $data->result();
						            }
      			
      public function getbarang_mentah($where)
  						          {
						      	
                                $this->db->select('barang_mentah.*, jenis_barang.deskripsi_barang,warna_barang.*');
                                $this->db->join('warna_barang','warna_barang.id_warna = barang_mentah.id_warna');
                                $this->db->join('jenis_barang','jenis_barang.id_jenis_barang = barang_mentah.id_jenis_barang');
                              	$this->db->where( $where);
                                return $data = $this->db->get('barang_mentah');
					

      				          }

      public function getRequest_barang()
  						          {
                                $this->db->select('barang_mentah.*,karyawan.username,jenis_barang.*,warna_barang.*,permintaan_barang.*');
                      	        $this->db->join('barang_mentah','barang_mentah.id_barang_mentah = permintaan_barang.id_barang_mentah');
                                $this->db->join('warna_barang','barang_mentah.id_warna = warna_barang.id_warna');
                      	        $this->db->join('jenis_barang','jenis_barang.id_jenis_barang = barang_mentah.id_jenis_barang');
                      	        $this->db->join('karyawan','karyawan.id_karyawan = permintaan_barang.id_karyawan');
                                $this->db->order_by('id_permintaan_barang', 'DESC');
                      	        $data = $this->db->get('permintaan_barang');
                                return $data->result();
						            }
      public function getKonfirm_barang()
                        {
                                $this->db->select('barang_mentah.*,jenis_barang.*,warna_barang.*,permintaan_barang.jumlah,pengiriman_barang_mentah.*');
                                $this->db->join('permintaan_barang','permintaan_barang.id_permintaan_barang= pengiriman_barang_mentah.id_permintaan_barang');
                                $this->db->join('barang_mentah','barang_mentah.id_barang_mentah = permintaan_barang.id_barang_mentah ');
                                $this->db->join('warna_barang','barang_mentah.id_warna = warna_barang.id_warna');
                                $this->db->join('jenis_barang','jenis_barang.id_jenis_barang = barang_mentah.id_jenis_barang');
                                  $this->db->order_by('id_permintaan_barang', 'DESC');
                                $data = $this->db->get('pengiriman_barang_mentah');
                                return $data->result();
                        }
      public function permintaan_oke ($id_pengiriman_barang)
                        {
                              
                               $this->db->query("UPDATE pengiriman_barang_mentah set status_pengiriman = 1  where id_pengiriman_barang = $id_pengiriman_barang ");
                               
                         
                        }
       public function permintaan_batal ($id_pengiriman_barang)
                        {
                              
                               $this->db->query("UPDATE pengiriman_barang_mentah set status_pengiriman = 0  where id_pengiriman_barang = $id_pengiriman_barang ");
                               
                         
                        }
      

}