     <?php
class Model_admin extends CI_Model{
        
 
    public function tampil_username(){

          $this->db->select("*");
          $this->db->from("karyawan");
          $karyawan=$_SESSION['id_karyawan'];
          $this->db->where('id_karyawan',$karyawan);
          $query=$this->db->get();
          return $query->result();
        }
  	public function getKaryawan($table)
  						{
						      	$data = $this->db->get($table);
						      	return $data->result_array();
      				}
    public function getbarangmentah($table)
  						{
						      	$data = $this->db->get($table);
						      	return $data->result_array();
      				}
    public function getjenisbarang($table)
              {
                    $data = $this->db->get($table);
                    return $data->result_array();
              }
    public function getwarna()
                      {
                                $data = $this->db->get('warna_barang');
                                return $data->result();
                      }
    public function getsupplier()
              {
                    $data = $this->db->get('supplier_barang');
                    return $data->result();
              }            
    public function input_data($data,$table)
    					{                           
                                $this->db->insert($table,$data);
                            
                        }                
    public function update_data($where,$data,$table)
                       {
                                $this->db->where($where);
                                $this->db->update($table,$data);
                        }
    public function hapus_data($where,$table){
                                $this->db->where($where);
                                $this->db->delete($table);
                        }
    public function edit_data($where,$table)
                        {
                                return $this->db->get_where($table,$where);
                        }
    public function valid_nameJenisbarang($id_jenis_barang)
    {
        $query = $this->db->get_where('jenis_barang', array('id_jenis_barang' => $id_jenis_barang));
        if ($query->num_rows() > 0)
          {
              return TRUE;
          }
        else
          {
              return FALSE;
          }
        echo $this->db->last_query();
    }

    public function valid_nameWarna($nama_warna)
    {
        $query = $this->db->get_where('warna_barang', array('warna' => $nama_warna));
        if ($query->num_rows() > 0)
          {
              return TRUE;
          }
        else
          {
              return FALSE;
          }
        echo $this->db->last_query();
    }
    
     public function valid_nameSupplier($nama_supplier)
    {
        $query = $this->db->get_where('supplier_barang', array('nama_supplier' => $nama_supplier));
        if ($query->num_rows() > 0)
          {
              return TRUE;
          }
        else
          {
              return FALSE;
          }
        echo $this->db->last_query();
    }
   


  }   
?>